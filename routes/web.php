<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\auth\AuthController;
use App\Http\Controllers\attendance\AttendanceController;
use App\Http\Controllers\event\EventController;

use App\Http\Controllers\ppdb\PpdbController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::controller(AuthController::class)->group(function () {
    Route::get('/login', 'login')->name('login');
    Route::get('/loginppdb', 'login_ppdb')->name('login-ppdb');
    Route::post('/authenticate', 'authenticate');
});

Route::controller(PpdbController::class)->group(function () {
    Route::get('/ppdb/home','index')->name('ppdb-home');
    Route::get('/ppdb/formulir','formulir')->name('ppdb-formulir');
});

Route::group(['middleware' => ['sessionVerify']], function() {
    # Dashboard ######################
        Route::get('/', function () {
            return view('dashboard');
        })->name('home');
    
    # Authentications ######################
        Route::controller(AuthController::class)->group(function () {
            Route::get('/logout', 'logout');
        });
    
    # Attendance ######################
        Route::resource('attendance',AttendanceController::class );

    # Event #####################
        Route::resource('event',EventController::class );

    Route::get('point', function () {
        return view('nilai/nilai');
    });

    Route::get('article', function () {
        return view('article/article');
    });
    
    Route::get('article/create', function () {
        return view('article/newArticle');
    });

    Route::get('schedule/create', function () {
        return view('schedule/newSchedule');
    });
    Route::get('schedule', function () {
        return view('schedule/schedule');
    });
    Route::get('events', function () {
        return view('eventSchedule/eventSchedule');
    });

    # Kurikulum #####################
        Route::get('curriculum', function () {
            return view('curriculum/curriculum');
        });
        Route::get('curriculum/subjects', function () {
            return view('curriculum/subjects/subjects');
        });
        Route::get('curriculum/subjects/create', function () {
            return view('curriculum/subjects/subjects_create');
        });
        Route::get('curriculum/subjects/edit', function () {
            return view('curriculum/subjects/subjects_edit');
        });

        Route::get('curriculum/chapters', function () {
            return view('curriculum/chapters/chapters');
        });
        Route::get('curriculum/chapters/create', function () {
            return view('curriculum/chapters/chapters_create');
        });
        Route::get('curriculum/chapters/edit', function () {
            return view('curriculum/chapters/chapters_edit');
        });

        Route::get('curriculum/materials', function () {
            return view('curriculum/materials/materials');
        });
        Route::get('curriculum/materials/create', function () {
            return view('curriculum/materials/materials_create');
        });
        Route::get('curriculum/materials/edit', function () {
            return view('curriculum/materials/materials_edit');
        });

    # Forum #####################
        Route::get('forum', function () {
            return view('forum/forum');
        });
        Route::get('forum/create', function () {
            return view('forum/forum_create');
        });
        Route::get('forum/edit', function () {
            return view('forum/forum_edit');
        });
        Route::get('forum/detail', function () {
            return view('forum/forum_detail');
        });

    Route::get('note', function () {
        return view('note/note');
    });

});

