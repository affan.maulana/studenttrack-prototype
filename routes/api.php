<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\auth\AuthAPIController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
    return "hello";
});

Route::post('/authenticate', [AuthAPIController::class, 'authenticate']);

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::controller(AuthAPIController::class)->group(function () {
        Route::post('/register', 'register');
        Route::post('/logout', 'logout');
    });

    Route::get('tes', function () {
        return "hello hello";
    });
});
