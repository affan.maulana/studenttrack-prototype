<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/studenttrack.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.css')}}">
    @yield('css')

    <style>
        .breadcrumb{
            background-color:#eeeeee!important;
            padding: 0.5rem !important;
            margin-top: 0.5rem !important;
        }
        .grey-font{
            color:#869099
        }
    </style>
</head>
<body class="hold-transition sidebar-mini sidebar-collapse layout-navbar-fixed">
    <!-- Site wrapper -->
    <div class="wrapper">
        @include('layout.navigation')
        <div class="content-wrapper">
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        @yield('head_content')
                    </div>
                </div>
            </section>

            @yield('content')
        </div>

        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Version</b> 0.1
            </div>
            <strong>studenttrack &copy; 2022</strong> 
        </footer>
    </div>

    <!-- SCRIPTS -->
    <script src="{{asset('/jquery/jquery.js')}}"></script>
    <script src="{{asset('bootstrap/js/bootstrap.bundle.js')}}"></script>
    <script src="{{asset('dist/js/adminlte.js')}}"></script>
    @yield('scripts')
</body>

</html>
