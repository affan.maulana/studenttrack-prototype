<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Studenttrack | Log in</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('fontawesome-free/css/all.css')}}">
  <!-- Theme style -->
<link rel="stylesheet" href="{{asset('dist/css/adminlte.css')}}">
<style>
  .alert {
  padding: 20px;
  background-color: #f44336;
  color: white;
}

.closebtn {
  margin-left: 15px;
  color: white;
  font-weight: bold;
  float: right;
  font-size: 22px;
  line-height: 20px;
  cursor: pointer;
  transition: 0.3s;
}

.closebtn:hover {
  color: black;
}
</style>
</head>
<body class="hold-transition login-page">
    @yield('content')
    
    <script src="{{asset('/jquery/jquery.js')}}"></script>
    <script src="{{asset('bootstrap/js/bootstrap.bundle.js')}}"></script>
    <script src="{{asset('dist/js/adminlte.js')}}"></script>
</body>
</html>


