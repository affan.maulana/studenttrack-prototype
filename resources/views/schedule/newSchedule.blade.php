@extends('layout.main')

@section('title', 'Jadwal | Buat Jadwal Mengajar')
@section('css')
<style>
    .square {
        height: 25px;
        width: 25px;
    }

    .big-checkbox {
        width: 200%;
        height: 80%;
    }

    td {
        height: 160px;
    }

    .col-1-min {
        padding-left: 7.5px;
        max-width: 4%;
    }

    .btn-xs {
        max-width: 150px;
    }

</style>
@endsection
@section('head_content')
<div class="col">
    <h1><i class="fas fa-clock"></i> Jadwal</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i></a></li>
        <li class="breadcrumb-item active">Jadwal</li>
    </ol>
</div>
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card p-2">
            <div class="row">
                <div class="col-sm-2">
                    <img src="{{asset('dist/img/logohere.png')}}" style="width:80px;height:80px;opacity: .8;"
                        class="ml-2 brand-image img-circle elevation-3 float-right">
                </div>
                <div class="col ml-2">
                    <h4>Nama Guru</h4>
                    <div class="row">
                        <div class="col">Wali kelas</div>
                        <div class="col">: 4-A</div>
                    </div>
                    <div class="row">

                        <div class="col">Matapelajaran</div>
                        <div class="col">: Fisika</div>
                    </div>
                </div>
                <div class="col">
                </div>

                <div class="col">
                </div>
                <div class="col">
                    <button type="button" class="btn btn-block btn-outline-info">Atur Jadwal Kegiatan</button>
                    <button type="button" class="btn btn-block btn-outline-info">Atur Jadwal Mengajar</button>
                </div>
            </div>
        </div>

        <div class="row p-2">
            <div class="col-2">
                <select class="custom-select form-control-border" id="exampleSelectBorder">
                    <option>Januari</option>
                    <option>Februari</option>
                    <option>Maret</option>
                    <option>April</option>
                    <option>Mei</option>
                    <option>Juni</option>
                    <option>Juli</option>
                    <option>Agustus</option>
                    <option>September</option>
                    <option selected>Oktober</option>
                    <option>November</option>
                    <option>Desemeber</option>
                </select>
            </div>
            <div class="col-2">
                <select class="custom-select form-control-border" id="exampleSelectBorder">
                    <option>2022</option>
                    <option>2021</option>
                    <option>2020</option>
                    <option>2019</option>
                </select>
            </div>
            <div class="col">
            </div>

            <div class="col align-middle">
                <div class="row">
                    <div class="col-1">
                        <input class="big-checkbox" checked id="cb-mengajar" type="checkbox">
                    </div>
                    <div class="col">
                        <label for="cb-mengajar"> Tampilkan Jadwal Mengajar</label>
                    </div>
                </div>
            </div>

            <div class="col align-middle">
                <div class="row">
                    <div class="col-1">
                        <input class="big-checkbox" checked id="cb-kegiatan" type="checkbox">
                    </div>
                    <div class="col">
                        <label for="cb-kegiatan"> Tampilkan Jadwal Kegiatan</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <table class="table table-bordered mb-2">
                    <thead>
                        <tr>
                            <th class="text-center">Senin</th>
                            <th class="text-center">Selasa</th>
                            <th class="text-center">Rabu</th>
                            <th class="text-center">Kamis</th>
                            <th class="text-center">Jumat</th>
                            <th class="text-center">Sabtu</th>
                            <th class="text-center">Minggu</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <span class="float-right">1</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">08:00 - 10:00 XII IPA
                                    1</button>
                            </td>
                            <td>
                                <span class="float-right">2</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">08:00 - 10:00 XII IPA
                                    1</button>
                            </td>
                            <td>
                                <span class="float-right">3</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">08:00 - 10:00 XII IPA
                                    1</button>
                            </td>
                            <td>
                                <span class="float-right">4</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">08:00 - 10:00 XII IPA
                                    1</button>

                            </td>
                            <td>
                                <span class="float-right">5</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">08:00 - 10:00 XII IPA
                                    1</button>

                            </td>
                            <td>
                                <span class="float-right">6</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">08:00 - 10:00 XII IPA
                                    1</button>

                            </td>
                            <td>
                                <span class="float-right">7</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">08:00 - 10:00 XII IPA
                                    1</button>

                            </td>
                        </tr>

                        <tr>
                            <td class="h-100">
                                <span class="float-right">8</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>

                            </td>
                            <td>
                                <span class="float-right">9</span>
                                <br>
                                <button type="button" class="btn btn-block btn-success btn-xs">
                                    10:00 - 12:00
                                    Rapat awal bulan orang tua XI IPA
                                </button>
                            </td>
                            <td>
                                <span class="float-right">10</span>
                                <br>
                                <button type="button" class="btn btn-block btn-success btn-xs">
                                    10:00 - 12:00 Rapat awal bulan orang tua XI IPA</button>
                            </td>
                            <td>
                                <span class="float-right">11</span>
                                <br>
                                <button type="button" class="btn btn-block btn-warning btn-xs">
                                    14:00 - 17:00 <br>
                                    Event Haloween SMA ..
                                </button>
                            </td>
                            <td>
                                <span class="float-right">12</span>
                                <br>
                                <button type="button" class="btn btn-block btn-warning btn-xs">
                                    14:00 - 17:00 <br>
                                    Event Haloween SMA ..
                                </button>
                            </td>
                            <td>
                                <span class="float-right">13</span>
                                <br>
                            </td>
                            <td>
                                <span class="float-right">14</span>
                                <br>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <span class="float-right">15</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>
                            </td>
                            <td>
                                <span class="float-right">16</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>

                            </td>
                            <td>
                                <span class="float-right">17</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>
                            </td>
                            <td>
                                <span class="float-right">18</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>
                            </td>
                            <td>
                                <span class="float-right">19</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>
                            </td>
                            <td>
                                <span class="float-right">20</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>
                            </td>
                            <td>
                                <span class="float-right">21</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <span class="float-right">22</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>
                            </td>
                            <td>
                                <span class="float-right">23</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>
                            </td>
                            <td>
                                <span class="float-right">24</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>
                            </td>
                            <td>
                                <span class="float-right">25</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>
                            </td>
                            <td>
                                <span class="float-right">26</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>
                            </td>
                            <td>
                                <span class="float-right">27</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>
                            </td>
                            <td>
                                <span class="float-right">28</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <span class="float-right">29</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>
                            </td>
                            <td>
                                <span class="float-right">30</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>
                            </td>
                            <td>
                                <span class="float-right">31</span>
                                <br>
                                <button type="button" class="btn btn-block btn-default btn-xs">
                                    08:00 - 10:00 XII IPA 1
                                </button>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                    </tbody>
                </table>
                <div class="row">
                    <div class="col-1-min">
                        <div class="square" style="background-color: #f8f9fa;border:1px solid #ddd"></div>
                    </div>
                    <div class="col">
                        jadwal Mengajar
                    </div>

                    <div class="col-1-min">
                        <div class="square" style="background-color: #28a745;"></div>
                    </div>
                    <div class="col">
                        jadwal Kegiatan Personal
                    </div>

                    <div class="col-1-min">
                        <div class="square" style="background-color: #ffc107;"></div>
                    </div>
                    <div class="col">
                        jadwal Kegiatan Umum
                    </div>
                    <div class="col">
                    </div>
                    <div class="col">
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection


@section('scripts')
<script>
    $(document).ready(function () {
        $("#sidebarCollapse > li").removeClass("active");
        $("#nav-jadwal > a").addClass("active");
    });

</script>
@endsection
