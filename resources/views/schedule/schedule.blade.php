@extends('layout.main')

@section('title', 'Jadwal | Jadwal Mengajar')
@section('css')
<style>
    #schedule-table {
        display: block;
        overflow-x: auto;
        white-space: nowrap;
    }

    #schedule-table>thead>tr>th {
        padding: 7px;
    }

    .vertical-center {
        position: absolute;
        top: 50%;
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
    }

</style>
<link rel="stylesheet" type="text/css" href="{{asset('datatables/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('datatables/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('datatables/buttons.bootstrap4.min.css')}}">

@endsection
@section('head_content')
<div class="col">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i></a></li>
        <li class="breadcrumb-item active">Jadwal Mengajar</li>
    </ol>
</div>
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card p-2">
            <div class="row">
                <div class="col-sm-2">
                    <img src="{{asset('dist/img/logohere.png')}}" style="width:80px;height:80px;opacity: .8;"
                        class="ml-2 brand-image img-circle elevation-3 float-right">
                </div>
                <div class="col ml-4">
                    <h4>Nama Guru</h4>
                    <div class="row">
                        <div class="col-sm-4"><span class="vertical-center">Tahun Ajaran</span></div>
                        <div class="col-sm-6">
                            <select class="custom-select form-control-border" id="yearSelect">
                                <option>2022/2023 - Ganjil</option>
                                <option>2022/2023 - Genap</option>
                                <option>2021/2022 - Ganjil</option>
                                <option>2021/2022 - Genap</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col"></div>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-block btn-outline-info">Atur Jadwal Mengajar</button>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <table id="schedule-table" class="table table-bordered mb-2">
                    <thead>
                        <tr>
                            <th class="bg-light w-100"></th>
                            <th>06:00</th>
                            <th>06:30</th>
                            <th>07:00</th>
                            <th>07:30</th>
                            <th>08:00</th>
                            <th>08:30</th>
                            <th>09:00</th>
                            <th>09:30</th>
                            <th>10:00</th>
                            <th>10:30</th>
                            <th>11:00</th>
                            <th>11:30</th>
                            <th>12:00</th>
                            <th>12:30</th>
                            <th>13:00</th>
                            <th>13:30</th>
                            <th>14:00</th>
                            <th>14:30</th>
                            <th>15:00</th>
                            <th>15:30</th>
                            <th>16:00</th>
                            <th>16:30</th>
                            <th>17:00</th>
                            <th>17:30</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <span>Senin</span>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td colspan="5">
                                <button type="button" class="btn btn-block btn-secondary btn-xs">
                                    XII IPA 2
                                </button>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td colspan="5">
                                <button type="button" class="btn btn-block btn-secondary btn-xs">
                                    XII IPA 2
                                </button>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>
                                <span>Selasa</span>
                            </td>
                            <td></td>
                            <td></td>
                            <td colspan="5">
                                <button type="button" class="btn btn-block btn-secondary btn-xs">
                                    XII IPA 3
                                </button>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td colspan="5">
                                <button type="button" class="btn btn-block btn-secondary btn-xs">
                                    XII IPA 3
                                </button>
                            </td>
                            <td></td>
                            <td></td>

                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>
                                <span>Rabu</span>
                            </td>
                            <td></td>
                            <td colspan="5">
                                <button type="button" class="btn btn-block btn-secondary btn-xs">
                                    XII IPA 3
                                </button>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td colspan="5">
                                <button type="button" class="btn btn-block btn-secondary btn-xs">
                                    XII IPA 3
                                </button>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>
                                <span>Kamis</span>
                            </td>
                            <td></td>
                            <td colspan="5">
                                <button type="button" class="btn btn-block btn-secondary btn-xs">
                                    XII IPA 3
                                </button>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td colspan="5">
                                <button type="button" class="btn btn-block btn-secondary btn-xs">
                                    XII IPA 3
                                </button>
                            </td>

                        </tr>

                        <tr>
                            <td>
                                <span>Kamis</span>
                            </td>
                            <td></td>
                            <td colspan="5">
                                <button type="button" class="btn btn-block btn-secondary btn-xs">
                                    XII IPA 3
                                </button>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td colspan="5">
                                <button type="button" class="btn btn-block btn-secondary btn-xs">
                                    XII IPA 3
                                </button>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>
                                <span>Jumat</span>
                            </td>
                            <td></td>
                            <td colspan="5">
                                <button type="button" class="btn btn-block btn-secondary btn-xs">
                                    XII IPA 3
                                </button>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td colspan="5">
                                <button type="button" class="btn btn-block btn-secondary btn-xs">
                                    XII IPA 3
                                </button>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>
                                <span class="text-danger">Sabtu</span>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>
                                <span class="text-danger">Minggu</span>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="card">
            <!-- <div class="card-header">
                <h3 class="card-title">DataTable with minimal features & hover style</h3>
            </div> -->
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Hari</th>
                            <th>Jam</th>
                            <th>Kelas</th>
                            <th>Kode</th>
                            <th>Matapelajaran</th>
                            <th>Pengajar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Senin</td>
                            <td>08:00</td>
                            <td>XII IPA 2</td>
                            <td>SMA239</td>
                            <td>Fisika Kelas 12</td>
                            <td>Affan Maulana</td>
                        </tr>
                        <tr>
                            <td>Senin</td>
                            <td>08:00</td>
                            <td>XII IPA 2</td>
                            <td>SMA239</td>
                            <td>Fisika Kelas 12</td>
                            <td>Affan Maulana</td>
                        </tr>
                        <tr>
                            <td>Senin</td>
                            <td>08:00</td>
                            <td>XII IPA 2</td>
                            <td>SMA239</td>
                            <td>Fisika Kelas 12</td>
                            <td>Affan Maulana</td>
                        </tr>
                        <tr>
                            <td>Senin</td>
                            <td>08:00</td>
                            <td>XII IPA 2</td>
                            <td>SMA239</td>
                            <td>Fisika Kelas 12</td>
                            <td>Affan Maulana</td>
                        </tr>
                        <tr>
                            <td>Senin</td>
                            <td>08:00</td>
                            <td>XII IPA 2</td>
                            <td>SMA239</td>
                            <td>Fisika Kelas 12</td>
                            <td>Affan Maulana</td>
                        </tr>
                        <tr>
                            <td>Senin</td>
                            <td>08:00</td>
                            <td>XII IPA 2</td>
                            <td>SMA239</td>
                            <td>Fisika Kelas 12</td>
                            <td>Affan Maulana</td>
                        </tr>
                        <tr>
                            <td>Senin</td>
                            <td>08:00</td>
                            <td>XII IPA 2</td>
                            <td>SMA239</td>
                            <td>Fisika Kelas 12</td>
                            <td>Affan Maulana</td>
                        </tr>
                        <tr>
                            <td>Senin</td>
                            <td>08:00</td>
                            <td>XII IPA 2</td>
                            <td>SMA239</td>
                            <td>Fisika Kelas 12</td>
                            <td>Affan Maulana</td>
                        </tr>
                        <tr>
                            <td>Senin</td>
                            <td>08:00</td>
                            <td>XII IPA 2</td>
                            <td>SMA239</td>
                            <td>Fisika Kelas 12</td>
                            <td>Affan Maulana</td>
                        </tr>
                        <tr>
                            <td>Senin</td>
                            <td>08:00</td>
                            <td>XII IPA 2</td>
                            <td>SMA239</td>
                            <td>Fisika Kelas 12</td>
                            <td>Affan Maulana</td>
                        </tr>
                        <tr>
                            <td>Senin</td>
                            <td>08:00</td>
                            <td>XII IPA 2</td>
                            <td>SMA239</td>
                            <td>Fisika Kelas 12</td>
                            <td>Affan Maulana</td>
                        </tr>
                        <tr>
                            <td>Senin</td>
                            <td>08:00</td>
                            <td>XII IPA 2</td>
                            <td>SMA239</td>
                            <td>Fisika Kelas 12</td>
                            <td>Affan Maulana</td>
                        </tr>
                        <tr>
                            <td>Senin</td>
                            <td>08:00</td>
                            <td>XII IPA 2</td>
                            <td>SMA239</td>
                            <td>Fisika Kelas 12</td>
                            <td>Affan Maulana</td>
                        </tr>
                        <tr>
                            <td>Senin</td>
                            <td>08:00</td>
                            <td>XII IPA 2</td>
                            <td>SMA239</td>
                            <td>Fisika Kelas 12</td>
                            <td>Affan Maulana</td>
                        </tr>
                    </tbody>
                    
                </table>
            </div>
            <!-- /.card-body -->
        </div>

    </div>
</section>
@endsection


@section('scripts')
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/dataTables.responsive.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/responsive.bootstrap4.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/dataTables.buttons.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $("#sidebarCollapse > li").removeClass("active");
        $("#nav-jadwal > a").addClass("active");

    });

    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });

</script>
@endsection
