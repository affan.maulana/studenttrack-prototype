@extends('layout.main')

@section('title', 'Jadwal')
@section('css')
<style>
    .bottom-button
    {
        position: absolute;
        width: 95%;
        bottom: 0;
    }
</style>
@endsection
@section('head_content')
<div class="col">
    <ol class="breadcrumb p-2">
        <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i></a></li>
        <li class="breadcrumb-item active">Kurikulum & Materi</li>
    </ol>
</div>
@endsection
@section('content')
<div class="card m-3 p-3">
    <div class="row">
        <div class="col-sm-2">
            <img src="{{asset('dist/img/logohere.png')}}" style="width:80px;height:80px;opacity: .8;"
                class="ml-2 brand-image img-circle elevation-3 float-right">
        </div>
        <div class="col">
            <h4>Matapelajaran</h4>
            <span>Manajemen Mata Pelajaran sesuai dengan kurikulum yang digunakan</span>
        </div>

        <div class="col-2">
            <a href="{{url('/curriculum/subjects')}}">
                <button type="button" class="btn btn-block btn-outline-info bottom-button">Atur Jadwal Kegiatan</button>
            </a>
        </div>
    </div>
</div>

<div class="card m-3 p-3">
    <div class="row">
        <div class="col-sm-2">
            <img src="{{asset('dist/img/logohere.png')}}" style="width:80px;height:80px;opacity: .8;"
                class="ml-2 brand-image img-circle elevation-3 float-right">
        </div>
        <div class="col">
            <h4>Bab & Modul</h4>
            <span>Manajemen Bab, Sub Bab dan Modul Pembelajaran sesuai dengan Mata Pelajaran yang dipilih</span>
        </div>

        <div class="col-2">
            <a href="{{url('/curriculum/chapters')}}">
                <button type="button" class="btn btn-block btn-outline-info bottom-button">Atur Jadwal Kegiatan</button>
            </a>
        </div>
    </div>
</div>

<div class="card m-3 p-3">
    <div class="row">
        <div class="col-sm-2">
            <img src="{{asset('dist/img/logohere.png')}}" style="width:80px;height:80px;opacity: .8;"
                class="ml-2 brand-image img-circle elevation-3 float-right">
        </div>
        <div class="col">
            <h4>Materi Pembahasan</h4>
            <span>Manajemen Materi Pembahasan sesuai dengan Modul pembahasan masing-masing Mata Pelajaran</span>
        </div>

        <div class="col-2">
            <a href="{{url('/curriculum/materials')}}">
                <button type="button" class="btn btn-block btn-outline-info bottom-button">Atur Jadwal Kegiatan</button>
            </a>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script>
    $(document).ready(function () {
        $("#sidebarCollapse > li").removeClass("active");
        $("#nav-kurikulum > a").addClass("active");
    });

</script>
@endsection
