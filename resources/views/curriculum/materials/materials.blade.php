@extends('layout.main')

@section('title', 'Materi Pembahasan')
@section('css')
<style>
    .bottom-button
    {
        position: absolute;
        width: 95%;
        bottom: 0;
    }
    .search-form > tbody > tr > td
    {
        padding:0.4rem!important;
    }
    .cb-mapel{
        width: 140%;
        height: 20px;
    }
    
</style>

<link rel="stylesheet" type="text/css" href="{{asset('datatables/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('datatables/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('datatables/buttons.bootstrap4.min.css')}}">
@endsection
@section('head_content')
<div class="col">
    <ol class="breadcrumb p-2">
        <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i></a></li>
        <li class="breadcrumb-item"><a href="{{url('/curriculum')}}">Kurikulum & Materi</a></li>
        <li class="breadcrumb-item active">Materi Pembahasan</li>
    </ol>
</div>
@endsection
@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <table class="search-form" class="mb-2">
                            <tr>
                                <td>Kode Matapelajaran</td>
                                <td><input type="text" class="form-control" placeholder=""></td>
                            </tr>
                            <tr>
                                <td>Bab</td>
                                <td>
                                    <select class="custom-select rounded-0">
                                        <option>Value 1</option>
                                        <option>Value 2</option>
                                        <option>Value 3</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Modul</td>
                                <td>
                                    <select class="custom-select rounded-0">
                                        <option>Value 1</option>
                                        <option>Value 2</option>
                                        <option>Value 3</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col">
                        <table class="search-form" class="mb-2">
                            <tr>
                                <td>Matapelajaran</td>
                                <td><input type="text" class="form-control" placeholder=""></td>
                            </tr>
                            <tr>
                                <td>Tingkat</td>
                                <td>
                                    <select class="custom-select rounded-0">
                                        <option>Value 1</option>
                                        <option>Value 2</option>
                                        <option>Value 3</option>
                                    </select>
                                </td>
                                
                            </tr>
                            <tr>
                                <td>Kelas</td>
                                <td>
                                    <select class="custom-select rounded-0">
                                        <option>Value 1</option>
                                        <option>Value 2</option>
                                        <option>Value 3</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col"></div>
                    <div class="col-md-2">
                        <a href="{{url('curriculum/materials/create')}}">
                            <button type="button" class="btn btn-block btn-success font-weight-bold">
                                <i class="fa-solid fa-plus"></i> Tambah Materi Pembahasan
                            </button>
                        </a>

                        <button type="button" class="btn btn-block btn-info bottom-button font-weight-bold">
                            <i class="fa-solid fa-magnifying-glass"></i> Cari
                        </button>
                    </div>

                </div>
               
            </div>
        </div>
        
        <div class="card">
            <div class="card-body">
                <div class="row mb-4 mr-1">
                    <div class="col"></div>
                    <div class="col">
                        <div class="row float-right w-50">
                            <div class="col">
                                <button type="button" class="btn btn-block btn-danger btn-sm font-weight-bold">
                                    Hapus
                                </button>
                            </div>
                            <div class="col">
                                <button type="button" class="btn btn-block btn-success btn-sm font-weight-bold">
                                    Ekspor Data
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th style="width:10px">
                                <input class="cb-mapel align-middle" id="checkall" type="checkbox">
                            </th>
                            <th>Kode</th>
                            <th>Matapelajaran</th>
                            <th>Bab</th>
                            <th>Modul</th>
                            <th>Dibuat/Diubah oleh</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <input class="cb-mapel align-middle" type="checkbox">
                            </td>
                            <td><a href="{{url('curriculum/materials/edit')}}">KMZWKT773</a></td>
                            <td>Fisika</td>
                            <td>SMA</td>
                            <td>VII</td>
                            <td>Administrator</td>
                        </tr>
                        <tr>
                            <td>
                                <input class="cb-mapel align-middle" type="checkbox">
                            </td>
                            <td><a href="{{url('curriculum/materials/edit')}}">WKT823IE</a></td>
                            <td>Kimia</td>
                            <td>SMP</td>
                            <td>VII</td>
                            <td>Administrator</td>
                        </tr>
                        <tr>
                            <td>
                                <input class="cb-mapel align-middle" type="checkbox">
                            </td>
                            <td><a href="{{url('curriculum/materials/edit')}}">BBSHE923</a></td>
                            <td>Biologi</td>
                            <td>SMP</td>
                            <td>VII</td>
                            <td>Administrator</td>
                        </tr>
                        <tr>
                            <td>
                                <input class="cb-mapel align-middle" type="checkbox">
                            </td>
                            <td><a href="{{url('curriculum/materials/edit')}}">WEFI7372</a></td>
                            <td>Matematika</td>
                            <td>SMP</td>
                            <td>VII</td>
                            <td>Administrator</td>
                        </tr>
                        <tr>
                            <td>
                                <input class="cb-mapel align-middle" type="checkbox">
                            </td>
                            <td><a href="{{url('curriculum/materials/edit')}}">WE223EGE</a></td>
                            <td>Fisika</td>
                            <td>SMA</td>
                            <td>VII</td>
                            <td>Administrator</td>
                        </tr>
                        <tr>
                            <td>
                                <input class="cb-mapel align-middle" type="checkbox">
                            </td>
                            <td><a href="{{url('curriculum/materials/edit')}}">WWE434S</a></td>
                            <td>Kimia</td>
                            <td>SMA</td>
                            <td>VII</td>
                            <td>Administrator</td>
                        </tr>
                    </tbody>
                    
                </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


@section('scripts')
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/dataTables.responsive.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/responsive.bootstrap4.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/dataTables.buttons.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $("#sidebarCollapse > li").removeClass("active");
        $("#nav-kurikulum > a").addClass("active");
    });

    var table = $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "autoWidth": false,
        "responsive": true,
        "order": [[1, 'desc']],
        "columnDefs": [{
            "targets": 0,
            "orderable": false
        }]
    });

    $("#checkall").change(function() {
        if(this.checked) {
            $( ".cb-mapel" ).prop( "checked", true );
        }else{
            $( ".cb-mapel" ).prop( "checked", false );
        }
    });

</script>
@endsection
