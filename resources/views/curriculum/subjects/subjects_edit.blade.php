@extends('layout.main')

@section('title', 'Matapelajaran | Edit')
@section('css')
<style>
    .bottom-button
    {
        position: absolute;
        width: 95%;
        bottom: 0;
    }
    #search-form > tbody > tr > td
    {
        padding:0.4rem!important;
    }
    .cb-mapel{
        width: 140%;
        height: 20px;
    }
    
</style>

<link rel="stylesheet" type="text/css" href="{{asset('datatables/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('datatables/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('datatables/buttons.bootstrap4.min.css')}}">
@endsection
@section('head_content')
<div class="col">
    <ol class="breadcrumb p-2">
        <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i></a></li>
        <li class="breadcrumb-item"><a href="{{url('/curriculum')}}">Kurikulum & Materi</a></li>
        <li class="breadcrumb-item"><a href="{{url('/curriculum/subjects')}}">Matapelajaran</a></li>
        <li class="breadcrumb-item active">Edit</li>
    </ol>
</div>
@endsection
@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <table id="search-form" class="mb-2">
                            <tbody>
                                <tr>
                                    <td>Kode Matapelajaran <span class="text-danger">*</span></td>
                                    <td class="w-75"><input type="text" class="form-control" placeholder=""></td>
                                </tr>
                                <tr>
                                    <td>Matapelajaran <span class="text-danger">*</span></td>
                                    <td><input type="text" class="form-control" placeholder=""></td>
                                </tr>
                                <tr>
                                    <td>Tingkat <span class="text-danger">*</span></td>
                                    <td>
                                        <select class="custom-select rounded-0">
                                            <option>Value 1</option>
                                            <option>Value 2</option>
                                            <option>Value 3</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Kelas <span class="text-danger">*</span></td>
                                    <td>
                                        <select class="custom-select rounded-0">
                                            <option>Value 1</option>
                                            <option>Value 2</option>
                                            <option>Value 3</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Jenis Matapelajaran <span class="text-danger">*</span></td>
                                    <td>
                                        <select class="custom-select rounded-0">
                                            <option>Value 1</option>
                                            <option>Value 2</option>
                                            <option>Value 3</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Semester</td>
                                    <td>
                                        <select class="custom-select rounded-0">
                                            <option>Value 1</option>
                                            <option>Value 2</option>
                                            <option>Value 3</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Catatan</td>
                                    <td>
                                        <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                    <span class="text-muted font-weight-light"> Maksimal 500 karakter</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <span class="text-danger">Kolom yang bertanda * wajib diisi</span>
                    </div>
                    <div class="col-md-2">
                        <a href="{{url('curriculum/subjects')}}">
                            <button type="button" class="btn btn-block btn-danger font-weight-bold">
                                 Kembali
                            </button>
                        </a>
                        <br>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-block btn-success font-weight-bold">
                           Ubah
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


@section('scripts')
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/dataTables.responsive.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/responsive.bootstrap4.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/dataTables.buttons.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $("#sidebarCollapse > li").removeClass("active");
        $("#nav-kurikulum > a").addClass("active");
    });

    var table = $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "autoWidth": false,
        "responsive": true,
        "order": [[1, 'desc']],
        "columnDefs": [{
            "targets": 0,
            "orderable": false
        }]
    });

    $("#checkall").change(function() {
        if(this.checked) {
            $( ".cb-mapel" ).prop( "checked", true );
        }else{
            $( ".cb-mapel" ).prop( "checked", false );
        }
    });

</script>
@endsection
