@extends('layout.main_auth')

@section('title', 'Studenttrack | Login')
@section('content')

<div class="col bg-login d-flex align-items-center vh-100" style="background-image: radial-gradient(#2E5DFC, #0028AF);">

    <div class="row">
        <div class="col-md-8 d-flex align-items-center" style="padding: 5%">
            <img style="width: 100%;" src="{{asset('dist/img/logo/studenttrackTr.png')}}" alt="studenttrack">
        </div>
        <div class="col-md-4">
            <div class="login-box col mx-auto"
                style="width:100%; top: 50%;-ms-transform: translateY(-50%);transform: translateY(-50%);">
                <div class="login-logo text-white">
                    <label>PPDB</label> <strong>Al - Halimiyah</strong>
                </div>
                <!-- /.login-logo -->
                <div class="card">
                    <div class="card-body login-card-body">
                        <p class="login-box-msg">Sign in to start your session</p>

                        <div class="input-group mb-3">
                            <input name="email" type="email" class="form-control" placeholder="Email">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input name="password" type="password" class="form-control" placeholder="Password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col">
                                <input style="height:16px;width:16px;" class="align-middle" type="checkbox"> Ingat Saya?
                            </div>
                            <div class="col">
                                <a href="" class="float-right"><span>Lupa Password?</span></a>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Masuk</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.login-box -->
@endsection