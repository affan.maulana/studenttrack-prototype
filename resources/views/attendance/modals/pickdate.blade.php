<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pilih Tanggal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <label class="col control-label">Pilih Tanggal</label>
                <div class="col">
                    <input type="text" class="form-control datetimepicker-input" id="dateChoose" />
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batalkan</button>
                <button type="button" class="btn btn-primary" onclick="changeDate('date')">Pilih</button>
            </div>
        </div>
    </div>
</div>
