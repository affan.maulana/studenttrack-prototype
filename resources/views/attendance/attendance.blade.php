@extends('layout.main')

@section('title', 'Absensi')

@section('css')
<style>
    .vertical-center {
        margin: 0;
        position: absolute;
        top: 50%;
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
    }

    .card-rounded {
        background-color: #f4f4f4;
    }


    .card {
        padding: 10px;
    }

    .big-checkbox-header {
        width: 57%;
        height: 80%;
    }

    .big-checkbox {
        width: 50%;
        height: 50%;
    }

</style>
@endsection

@section('head_content')
<div class="col">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i></a></li>
        <li class="breadcrumb-item active">Absensi</li>
    </ol>
</div>
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="col-sm-5 ">
                <div class="form-group row">
                    <div class="col-sm-2">
                        <label for="tanggal" class="col-form-label">Tanggal</label>
                    </div>
                    <div class="col">
                        <div class="input-group date" id="pickdate1" data-target-input="nearest">
                            <div class="input-group-append" data-target="#pickdate1" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                            <input type="text" class="form-control datetimepicker-input" data-target="#pickdate1" />
                        </div>
                    </div>
                    <div class="col"></div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-2">
                        <label for="kelasInput" class="col-form-label">Kelas</label>
                    </div>
                    <div class="col">
                        <select class="custom-select form-control-border border-width-2" id="kelasInput">
                            <option>7 - A</option>
                            <option>7 - B</option>
                            <option>7 - C</option>
                        </select>
                    </div>
                    <div class="col"></div>
                </div>
                <button type="submit" class="btn btn-info"><i class="fas fa-search"></i> Cari</button>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">7 data found</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0 mb-3">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>NIS</th>
                            <th>Nama</th>
                            <th class="w-25">Presensi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1.</td>
                            <td>10003734</td>
                            <td>Affan Maulana</td>
                            <td>
                                <div class="row">
                                    <div class="col">
                                        <select class="custom-select form-control-border border-width-2">
                                            <option>Hadir</option>
                                            <option>Sakit</option>
                                            <option>Alpa</option>
                                        </select>
                                    </div>
                                    <div class="col-4">
                                       
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td>10003735</td>
                            <td>Joko Anwar</td>
                            <td>
                                <div class="row">
                                    <div class="col">
                                        <select class="custom-select form-control-border border-width-2">
                                            <option>Hadir</option>
                                            <option selected>Sakit</option>
                                            <option>Alpa</option>
                                        </select>
                                    </div>
                                    <div class="col-4">
                                        <button type="submit" class="btn btn-success rounded-circle">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>3.</td>
                            <td>10003736</td>
                            <td>Wawan Racing</td>
                            <td>
                                <div class="row">
                                    <div class="col">
                                        <select class="custom-select form-control-border border-width-2">
                                            <option>Hadir</option>
                                            <option selected>Sakit</option>
                                            <option>Alpa</option>
                                        </select>
                                    </div>
                                    <div class="col-4">
                                        <button type="submit" class="btn btn-success rounded-circle">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>4.</td>
                            <td>10003737</td>
                            <td>Andry tanu</td>
                            <td>
                                <div class="row">
                                    <div class="col">
                                        <select class="custom-select form-control-border border-width-2">
                                            <option>Hadir</option>
                                            <option selected>Sakit</option>
                                            <option>Alpa</option>
                                        </select>
                                    </div>
                                    <div class="col-4">
                                        <button type="submit" class="btn btn-warning rounded-circle">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>5.</td>
                            <td>10003737</td>
                            <td>Wanwan</td>
                            <td>
                                <div class="row">
                                    <div class="col">
                                        <select class="custom-select form-control-border border-width-2">
                                            <option>Hadir</option>
                                            <option >Sakit</option>
                                            <option selected>Alpa</option>
                                        </select>
                                    </div>
                                    <div class="col-4">
                                        <button type="submit" class="btn btn-warning rounded-circle">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>6.</td>
                            <td>10003737</td>
                            <td>Zilong</td>
                            <td>
                                <div class="row">
                                    <div class="col">
                                        <select class="custom-select form-control-border border-width-2">
                                            <option>Hadir</option>
                                            <option>Sakit</option>
                                            <option>Alpa</option>
                                        </select>
                                    </div>
                                    <div class="col-4">
                                        
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>7.</td>
                            <td>10003737</td>
                            <td>Yu Zhong</td>
                            <td>
                                <div class="row">
                                    <div class="col">
                                        <select class="custom-select form-control-border border-width-2">
                                            <option>Hadir</option>
                                            <option>Sakit</option>
                                            <option>Alpa</option>
                                        </select>
                                    </div>
                                    <div class="col-4">
                                        
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <ul class="pagination pagination-sm m-0 float-right">
                    <li class="page-item"><a class="page-link" href="#">«</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
@include('attendance.modals.pickdate')
@endsection

@section('scripts')
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/plugins/moment.min.js')}}"></script>
<script src="{{asset('/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $("#sidebarCollapse > li").removeClass("active");
        $("#nav-absen > a").addClass("active");
        /* Date picker */
        $('#pickdate1').datetimepicker({
            format: 'L'
        });
    });

    $(".click-row").click(function () {
        if ($(this).parent().find("input").is(":checked")) {
            $(this).parent().find("input").prop('checked', false);
        } else {
            $(this).parent().find("input").prop('checked', true);
        }
    });

    $('[data-toggle="tooltip"]').tooltip()

    function changeDate(param) {
        console.log(param)
    }

</script>
@endsection
