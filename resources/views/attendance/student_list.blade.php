@extends('layout.main')

@section('title', 'Absensi')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('datatables/jquery.dataTables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('datatables/select.dataTables.css')}}">
    <style>
        .checkbox{
            width:5%;
        }
    </style>
@endsection

@section('head_content')
    <div class="col-sm-6">
        <h1>Daftar Siswa</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{url('/')}}">Absensi</a></li>
            <li class="breadcrumb-item active">Daftar Siswa</li>
        </ol>
    </div>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="mx-2 my-2">
                    <button class="btn btn-danger">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <div class="col-12">
                    <table id="absensiTable" class="display">
                        <thead>
                            <tr>
                                <th class="checkbox text-center"> <input type="checkbox" class="select-checkbox" id="selectAll"> </th>
                                <th>Kelas</th>
                                <th>note</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach ($data['list_kelas'] as $kelas)
                           <tr>
                                <td></td>
                                <td>{{$kelas->name}}</td>
                                <td>sudah terisi</td>
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="mx-2 my-2">
                    <button class="btn btn-danger">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script type="text/javascript" charset="utf8" src="{{asset('/datatables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" charset="utf8" src="{{asset('/datatables/select.dataTables.js')}}"></script>
    <script>
        $(document).ready(function () {
            var table = $('#absensiTable').DataTable( {
                columnDefs: [ {
                    orderable: false,
                    className: 'select-checkbox',
                    targets:   0
                } ],
                select: {
                    style:    'multi',
                    selector: 'td:first-child'
                },
                order: [[ 1, 'asc' ]]
            } );
            
            $("#selectAll").on( "click", function(e) {
                if ($(this).is( ":checked" )) {
                    table.rows(  ).select();        
                } else {
                    table.rows(  ).deselect(); 
                }
            });
        
        });

        

    </script>
@endsection