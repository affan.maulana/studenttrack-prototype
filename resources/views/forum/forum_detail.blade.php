@extends('layout.main')

@section('title', 'Forum | Detail')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/summernote/summernote-bs4.min.css')}}">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<style>
    .note-editable{
        height:300px;
    }
    .img-profile{
        width:50px;
        height:50px;
        opacity: .8;
    }
     
</style>
@endsection
@section('head_content')
<div class="col">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i></a></li>
        <li class="breadcrumb-item"><a href="{{url('forum')}}">Forum</a></li>
        <li class="breadcrumb-item active">Forum Detail</li>
    </ol>
</div>
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-sm-1">
                        <img src="{{asset('dist/img/logohere.png')}}"
                            class="ml-2 brand-image img-circle elevation-3 float-right img-profile">
                    </div>
                    <div class="col">
                        <h4 class="mb-0">Indri Kusuma</h4>
                        Anggota <span class="font-weight-light">16/10/2022 15:00:21</span>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <h3>Info kegiatan sosial SMA Paramita</h3>
                Halo semua, <br>
                untuk tahun 2022 ini apakah SMA Paramita akan mengadakan kegiatan bakti sosial kembali seperti tahun-tahun sebelumnya? <br><br>
                Kalau boleh usul, untuk tahun ini kegiatan sosial dapat menyasar ke panti-panti asuhan. Ataupun jika ada usulan lain dapat kita diskusikan bersama di thread ini. <br><br>
                Terima kasih. <br>
                <span class="float-right font-weight-light">Diubah oleh <span class="font-weight-normal">Indri Kusuma</span> 16/10/2022 15:00:21</span>
            </div>
            <div class="card-footer">
                <div class="float-right">
                    <a href="#" class="text-dark mr-3"><i class="fa-solid fa-quote-left"></i> Kutip</a>
                    <a href="#" class="text-dark mr-3"><i class="fa-solid fa-reply"></i> Balas</a>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-sm-1">
                        <img src="{{asset('dist/img/logohere.png')}}"
                            class="ml-2 brand-image img-circle elevation-3 float-right img-profile">
                    </div>
                    <div class="col">
                        <h4 class="mb-0">Indri Kusuma</h4>
                        Anggota <span class="font-weight-light">16/10/2022 15:00:21</span>
                    </div>
                </div>
            </div>
            <div class="card-body">
                quote :
                <div class="card">
                    <div class="card-body bg-light">
                        Halo semua, <br>
                        untuk tahun 2022 ini apakah SMA Paramita akan mengadakan kegiatan bakti sosial kembali seperti tahun-tahun sebelumnya? <br><br>
                        Kalau boleh usul, untuk tahun ini kegiatan sosial dapat menyasar ke panti-panti asuhan. Ataupun jika ada usulan lain dapat kita diskusikan bersama di thread ini. <br><br>
                        Terima kasih. 
                    </div>
                </div>
                Usul dari saya, selain ke panti asuhan, bisa juga kita ke rumah lansia untuk memberikan bantuan, ada beberapa rumah lansia
disekitar SMA Paramita. Jika mau melakukan survei terlebih dahulu dapat kita rencanakan di minggu depan.<br>
                <span class="float-right font-weight-light">Diubah oleh <span class="font-weight-normal">Indri Kusuma</span> 16/10/2022 15:00:21</span>
            </div>
            <div class="card-footer">
                <div class="float-right">
                    <a href="#" class="text-dark mr-3"><i class="fa-solid fa-quote-left"></i> Kutip</a>
                    <a href="#" class="text-dark mr-3"><i class="fa-solid fa-reply"></i> Balas</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


@section('scripts')
<script src="{{asset('/plugins/moment.min.js')}}"></script>
<script src="{{asset('/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script src="{{asset('/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $("#sidebarCollapse > li").removeClass("active");
        $("#nav-forum > a").addClass("active");
        $('#summernote').summernote()
    });

   

</script>
@endsection