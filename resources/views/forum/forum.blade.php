@extends('layout.main')

@section('title', 'Forum')
@section('css')
<style>
    .card-header-span
    {
        bottom: 0px;
        position: absolute;
    }

    .fa-pen-to-square{
        color:#b4b450;
    }

    .fa-trash{
        color:red;
    }

    .fa-lock{
        color:black;
    }

    .fa-thumbtack{
        transform: rotate(45deg);
    }

</style>
@endsection
@section('head_content')
<div class="col">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i></a></li>
        <li class="breadcrumb-item active">Forum</li>
    </ol>
</div>
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
        <a href="{{url('forum/create')}}"><button type="submit" class="btn btn-info mb-3">Buat Thread Baru</button></a>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col">
                        <span class="card-header-span">Daftar Thread</span>
                    </div>
                    <div class="col-md-4">
                        <input name="search-thread-list" type="text" class="form-control form-control-sm" placeholder="Pencarian thread">
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Judul Thread</th>
                            <th>Tanggal Terbit</th>
                            <th>Pembuat</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><a href="{{url('forum/detail')}}">Peraturan forum SMA Paramita</a> <i class="fa-solid fa-thumbtack"></i></td>
                            <td>01/10/2022</td>
                            <td>Budi Permana </td>
                            <td>
                                <div class="row">
                                    <div class="col-md-3"><a href="{{url('forum/edit')}}"><i class="fa-regular fa-pen-to-square"></i></a></div>
                                    <div class="col-md-3"><a href="#"><i class="fa-solid fa-trash"></i></a></div>
                                    <div class="col-md-3"><a href="#"><i class="fa-solid fa-lock"></i></a></div>
                                </div>    
                            </td>
                        </tr>
                        <tr>
                            <td><a href="{{url('forum/detail')}}">Peraturan SMA Paramita *Update</a> <i class="fa-solid fa-thumbtack"></i></td>
                            <td>01/10/2022</td>
                            <td>Budi Permana </td>
                            <td>
                                <div class="row">
                                    <div class="col-md-3"><a href="{{url('forum/edit')}}"><i class="fa-regular fa-pen-to-square"></i></a></div>
                                    <div class="col-md-3"><a href="#"><i class="fa-solid fa-trash"></i></a></div>
                                    <div class="col-md-3"><a href="#"><i class="fa-solid fa-lock"></i></a></div>
                                </div>   
                            </td>
                        </tr>
                        <tr>
                            <td><a href="{{url('forum/detail')}}">Kegiatan Akhir Tahun Ajaran 2022 - Pengumpulan Ide</a></td>
                            <td> 01/10/2022</td>
                            <td>Budi Permana </td>
                            <td>
                                <div class="row">
                                    <div class="col-md-3"><a href="{{url('forum/edit')}}"><i class="fa-regular fa-pen-to-square"></i></a></div>
                                    <div class="col-md-3"><a href="#"><i class="fa-solid fa-trash"></i></a></div>
                                    <div class="col-md-3"><a href="#"><i class="fa-solid fa-lock"></i></a></div>
                                </div>    
                            </td>
                        </tr>
                        <tr>
                            <td><a href="{{url('forum/detail')}}">Sosialisasi Ujian Nasional Kurikulum Merdeka 2022</a></td>
                            <td>01/10/2022</td>
                            <td>Budi Permana </td>
                            <td>
                                <div class="row">
                                    <div class="col-md-3"><a href="{{url('forum/edit')}}"><i class="fa-regular fa-pen-to-square"></i></a></div>
                                    <div class="col-md-3"><a href="#"><i class="fa-solid fa-trash"></i></a></div>
                                    <div class="col-md-3"><a href="#"><i class="fa-solid fa-lock"></i></a></div>
                                </div>    
                            </td>
                        </tr>
                        <tr>
                            <td><a href="{{url('forum/detail')}}">Permintaan penambahan ekstrakulikuler</a></td>
                            <td>01/10/2022</td>
                            <td>Budi Permana </td>
                            <td>
                                <div class="row">
                                    <div class="col-md-3"><a href="{{url('forum/edit')}}"><i class="fa-regular fa-pen-to-square"></i></a></div>
                                    <div class="col-md-3"><a href="#"><i class="fa-solid fa-trash"></i></a></div>
                                    <div class="col-md-3"><a href="#"><i class="fa-solid fa-lock"></i></a></div>
                                </div>    
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                    <li class="page-item"><a class="page-link" href="#">«</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col" class="card-header-span">
                        <span class="card-header-span">Thread Teraktif</span>
                    </div>
                    <div class="col-md-4">
                        <input name="search-thread-aktif" type="text" class="form-control form-control-sm" placeholder="Pencarian thread">
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Judul Thread</th>
                            <th style="width:15%">Pembaruan Terakhir</th>
                            <th style="width:15%">Pembuat</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><a href="{{url('forum/detail')}}">Ujian Praktikum Olahraga</a> <i class="fa-solid fa-bookmark"></i></td>
                            <td>01/10/2022</td>
                            <td>Budi Permana </td>
                        </tr>
                        <tr>
                            <td><a href="{{url('forum/detail')}}">Voting liburan November 2022</a></td>
                            <td>01/10/2022</td>
                            <td>Budi Permana </td>
                        </tr>
                        <tr>
                            <td><a href="{{url('forum/detail')}}">Usulan rapat orang tua murid kelas XI</a></td>
                            <td>01/10/2022</td>
                            <td>Budi Permana </td>
                        </tr>
                        <tr>
                            <td><a href="{{url('forum/detail')}}">Usulan fitur pelacakan di aplikasi android studenttrack</a></td>
                            <td>01/10/2022</td>
                            <td>Budi Permana </td>
                        </tr>
                        <tr>
                            <td><a href="{{url('forum/detail')}}">Info kegiatan sosial SMA Paramita</a></td>
                            <td>01/10/2022</td>
                            <td>Budi Permana </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                    <li class="page-item"><a class="page-link" href="#">«</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col" class="card-header-span">
                        <span class="card-header-span">Thread Saya</span>
                    </div>
                    <div class="col-md-4">
                        <input name="search-thread-saya" type="text" class="form-control form-control-sm" placeholder="Pencarian thread">
                    </div>
                </div>
            </div>
            <div class="card-body">
            <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Judul Thread</th>
                            <th style="width:15%">Pembaruan Terakhir</th>
                            <th style="width:15%">Pembuat</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><a href="{{url('forum/detail')}}">Sosialisasi Ujian Nasional Kurikulum Merdeka 2022</a></td>
                            <td>01/10/2022</td>
                            <td>Budi Permana </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@endsection


@section('scripts')
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/dataTables.responsive.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/responsive.bootstrap4.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="{{asset('/datatables/dataTables.buttons.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $("#sidebarCollapse > li").removeClass("active");
        $("#nav-forum > a").addClass("active");
    });

    var table = $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "autoWidth": false,
        "responsive": true,
        "order": [[1, 'desc']],
        "columnDefs": [{
            "targets": 0,
            "orderable": false
        }]
    });

</script>
@endsection