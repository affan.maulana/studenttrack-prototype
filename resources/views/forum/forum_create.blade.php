@extends('layout.main')

@section('title', 'Forum | Buat Baru')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/summernote/summernote-bs4.min.css')}}">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<style>
    .note-editable{
        height:300px;
    }
</style>
@endsection
@section('head_content')
<div class="col">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i></a></li>
        <li class="breadcrumb-item"><a href="{{url('forum')}}">Forum</a></li>
        <li class="breadcrumb-item active">Buat Forum</li>
    </ol>
</div>
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                Thread Baru
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="form-judul">Judul</label>
                    <input type="text" class="form-control" id="form-judul" placeholder="Judul Thread">
                </div>
                <div class="form-group">
                    <label for="form-judul">Deskripsi</label>
                    <textarea id="summernote">
                        
                    </textarea>
                </div>
                <div class="row">
                    <div class="col">
                        <input style="height:16px;width:16px;" class="align-middle" type="checkbox"> Perbolehkan Balasan
                    </div>
                    <div class="col">
                        <a href="#" class="float-right mr-2"><button type="submit"  class="btn btn-danger mb-3">Batalkan</button></a>
                        <a href="#" class="float-right mr-2"><button type="submit" class="btn btn-warning mb-3">Pratinjau</button></a>
                        <a href="#" class="float-right mr-2"><button type="submit" class="btn btn-success mb-3">Terbitkan</button></a>

                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>
@endsection


@section('scripts')
<script src="{{asset('/plugins/moment.min.js')}}"></script>
<script src="{{asset('/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script src="{{asset('/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $("#sidebarCollapse > li").removeClass("active");
        $("#nav-forum > a").addClass("active");
        $('#summernote').summernote()
    });

   

</script>
@endsection