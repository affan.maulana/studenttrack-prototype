@extends('layout.main')

@section('title', 'Artikel')
@section('css')
<style>
    .vertical-center {
        margin: 0;
        top: 50%;
        -ms-transform: translateY(-50%);
        transform: translateY(-50%);
    }
    .tags{
        background-color:#FBE122; 
        border-radius: 0px 0px 100px 0px;
    }
</style>
@endsection
@section('head_content')
<div class="col">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i></a></li>
        <li class="breadcrumb-item active">Artikel</li>
    </ol>
</div>
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card p-2">
                    <div class="row">
                        <button class="btn btn-info ml-2" type="button" data-toggle="collapse" 
                            data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
                            <i class="fas fa-filter"></i> Filter
                        </button>

                        <a href="{{url('/article/create')}}"><button type="submit"  class="btn btn-info ml-2">
                            <i class="fas fa-plus-square"></i> Buat Baru
                        </button> </a>
                        
                    </div>
                    <div class="collapse col-sm-5 p-2 mt-2" id="collapseFilter">
                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="kelasInput" class="col-form-label">Judul</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control" placeholder="Cari Judul">
                            </div>
                            <div class="col"></div>

                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="tanggal" class="col-form-label">Tanggal</label>
                            </div>
                            <div class="col">
                                <div class="input-group date" id="pickdate1" data-target-input="nearest">
                                    <div class="input-group-append" data-target="#pickdate1" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                    <input type="text" class="form-control datetimepicker-input" placeholder="YYYY / MM / DD" data-target="#pickdate1"/>
                                </div>
                            </div>
                            <div class="col"></div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="kelasInput" class="col-form-label">Tags</label>
                            </div>
                            <div class="col">
                                <select class="custom-select form-control-border border-width-2" id="kelasInput">
                                    <option></option>
                                    <option>7 - A</option>
                                    <option>7 - B</option>
                                    <option>7 - C</option>
                                </select>
                            </div>
                            <div class="col"></div>
                        </div>
                        <button type="submit" class="btn btn-info"><i class="fas fa-search"></i> Cari</button>
                        <button type="submit" class="btn btn-info"><i class="fas fa-undo"></i> Reset</button>
                    </div>
                </div>

                <?php $data = 2;?>
                @for ($x = 0; $x <= $data; $x++)
                <div class="card">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('dist/img/hdimage.jpg')}}" style="max-width:100%;max-height:100%" alt="alhalimiyah">
                        </div>
                        <div class="col p-3">
                            <div class="p-1 w-25 tags mb-2">
                                <span class="ml-4 font-weight-bold">
                                    Tags
                                </span>
                            </div>
                            
                            <h4 class="mb-3">
                                Cheerful model sitting on the floor, wearing modern oversize black dresss 
                            </h4>
                            <div class="row mb-4">
                                <div class="col">
                                    <img style="width:25%" src="{{asset('dist/img/logohere.png')}}" 
                                        alt="alhalimiyah" class="brand-image img-circle elevation-3" style="opacity: .8">
                                    <span class="ml-2"> Author</span>
                                </div>
                                <div class="col col-form-label">
                                    <i class="far fa-comments"></i>
                                    <span>25 Comments</span>
                                </div>
                                <div class="col col-form-label">
                                    <i class="far fa-clock"></i>
                                    <span>Jan 13, 2022</span>
                                </div>
                                <div class="col"></div>
                                <div class="col"></div>
                            </div>
                            <span class="font-weight-light">
                                Fusce ac pharetra urna. Duis non lacus sit amet lacus interdum facilisis sed non est. Ut mi metus, semper eu dictum nec, condimentum sed sapien.
                            </span>
                        </div>
                    </div>

                </div>
                @endfor
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script src="{{asset('/plugins/moment.min.js')}}"></script>
<script src="{{asset('/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $("#sidebarCollapse > li").removeClass("active");
        $("#nav-artikel > a").addClass("active");

        /* Date picker */
        $('#pickdate1').datetimepicker({
            format: 'L'
        });
    });

    $('[data-toggle="tooltip"]').tooltip()
    function changeDate(param) {
        console.log(param)
    }

</script>
@endsection
