@extends('layout.main')

@section('title', 'Artikel | Buat Baru')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/summernote/summernote-bs4.min.css')}}">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<style>
    .note-editable{
        height:300px;
    }
</style>
@endsection
@section('head_content')
<div class="col">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i></a></li>
        <li class="breadcrumb-item"><a href="{{url('/article')}}">Artikel</a></li>
        <li class="breadcrumb-item active">Buat Artikel Baru</li>
    </ol>
</div>
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                Buat Artikel Baru
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label>Pilih Tag</label>
                    <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                        <option></option>
                        <option>Pengumuman</option>
                        <option>Acara</option>
                        <option>Info</option>
                        <option>Artikel</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="judul-text">Judul</label>
                    <input type="text" class="form-control" id="judul-text" placeholder="Judul Artikel">
                </div>
                <div class="form-group">
                    <label>Konten</label>
                    <textarea id="summernote"></textarea>
                </div>
                <div class="form-group">
                    <label for="judul-text">Kesimpulan</label>
                    <input type="text" class="form-control" id="judul-text" placeholder="Kesimpulan Artikel">
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col">
                        <input style="height:16px;width:16px;" id="cb-perbolehkan" class="align-middle" type="checkbox"> <label for="cb-perbolehkan">Perbolehkan Balasan</label>
                    </div>
                    <div class="col">
                        <a href="#" class="float-right mr-2"><button type="submit" class="btn btn-success">Terbitkan</button></a>
                        <a href="#" class="float-right mr-2"><button type="submit" class="btn btn-warning">Pratinjau</button></a>
                        <a href="#" class="float-right mr-2"><button type="submit"  class="btn btn-danger">Batalkan</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script src="{{asset('/plugins/moment.min.js')}}"></script>
<script src="{{asset('/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script src="{{asset('/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('#summernote').summernote()
    });
</script>
@endsection
