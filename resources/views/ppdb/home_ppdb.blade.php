@extends('ppdb.layout.main_ppdb')

@section('title', 'Home')
@section('css')
<style>
.vl {
    border-left: 2px solid #a8a8a8;
    height: 70px;
    margin-left: 6px;
}

.success-color {
    color: green;
}

.ongoing-color {
    color: grey;
}

.failed-color {
    color: red;
}

table td,
table td * {
    vertical-align: top;
}
</style>
@endsection
@section('head_content')
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-md-4 py-2">
                <div class="p-3 border rounded bg-white">
                    <h2>SMA Al - Halimiyah</h2>
                    <div class="row mt-2">
                        <div class="col-xs-1">
                            <i class="success-color fa-regular fa-circle-check"></i>
                            <div class="vl"></div>
                        </div>
                        <div class="col">
                            <strong class="success-color">Formulir</strong> <br>
                            8 Januari 2023 <br>
                            12:30
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xs-1">
                            <i class="success-color fa-regular fa-circle-check"></i>
                            <div class="vl"></div>
                        </div>
                        <div class="col">
                            <strong class="success-color">Verifikasi Formulir</strong> <br>
                            8 Januari 2023 <br>
                            12:30
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xs-1">
                            <i class="success-color fa-regular fa-circle-check"></i>
                            <div class="vl"></div>
                        </div>
                        <div class="col">
                            <strong class="success-color">Wawancara Orang Tua</strong> <br>
                            8 Januari 2023 <br>
                            12:30
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xs-1">
                            <i class="success-color fa-regular fa-circle-check"></i>
                            <div class="vl"></div>
                        </div>
                        <div class="col">
                            <strong class="success-color">Ujian Seleksi</strong> <br>
                            8 Januari 2023 <br>
                            12:30
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xs-1">
                            <i class="failed-color fa-solid fa-circle-xmark"></i>
                            <div class="vl"></div>
                        </div>
                        <div class="col">
                            <strong class="failed-color">Hasil Ujian Seleksi</strong> <br>
                            8 Januari 2023 <br>
                            12:30
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xs-1">
                            <i class="ongoing-color fa-solid fa-circle-minus"></i>
                            <div class="vl ongoing-color"></div>
                        </div>
                        <div class="col">
                            <strong class="ongoing-color">Registrasi Ulang</strong> <br>
                            8 Januari 2023 <br>
                            12:30
                        </div>
                    </div>
                </div>
            </div>
            <div class="col py-2">
                <div class="p-3 border rounded bg-white">
                    <h2>Biodata Diri</h2>
                    <div class="col">
                        <table>
                            <tr>
                                <td class="pr-3" rowspan="7" width="100px">
                                    <img width=170 src="{{asset('dist/img/user8-128x128.jpg')}}">
                                </td>
                            </tr>
                            <tr>
                                <td>Nomor Registrasi</td>
                                <td>: <strong>999898989</strong></td>
                            </tr>
                            <tr>
                                <td>Nama Lengkap</td>
                                <td>: <strong>Affan Maulana</strong></td>
                            </tr>
                            <tr>
                                <td>Tanggal Lahir</td>
                                <td>: <strong>24 / 09 / 2007</strong></td>
                            </tr>
                            <tr>
                                <td>Jenis Kelamin</td>
                                <td>: <strong>Laki - Laki</strong></td>
                            </tr>
                            <tr>
                                <td>Sekolah Asal</td>
                                <td>: <strong>SMP 199</strong></td>
                            </tr>
                            <tr>
                                <td>Kota Asal</td>
                                <td>: <strong>Bekasi Barat</strong></td>
                            </tr>
                        </table>
                    </div>
                    <table>

                        <tr>
                            <td>Nomor Telpon</td>
                            <td>: <strong>0812833849</strong></td>
                        </tr>
                        <tr>
                            <td>Agama</td>
                            <td>: <strong>Islam</strong></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Gelombang/Batch</td>
                            <td>: <strong>2 (Dua)</strong></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@endsection

@section('scripts')
<script>
$(document).ready(function() {
    $("#sidebarCollapse > li").removeClass("active");
    $("#nav-home > a").addClass("active");

    /* Date picker */
    $('#pickdate1').datetimepicker({
        format: 'L'
    });
});

$('[data-toggle="tooltip"]').tooltip()

function changeDate(param) {
    console.log(param)
}
</script>
@endsection