@extends('ppdb.layout.main_ppdb')

@section('title', 'Formulir PPDB')
@section('css')
<link rel="stylesheet" href="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">
<style>
    .input-group-addon {
    padding: 6px 12px;
    font-size: 14px;
    font-weight: 400;
    line-height: 1;
    color: #555;
    text-align: center;
    background-color: #eee;
    border: 1px solid #ccc;
    border-radius: 4px;
    border-radius: 0;
    border-color: #d2d6de;
    background-color: #fff;

}
.input-group-addon:first-child {
    border-right: 0;
}
.fa {
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
.circle {
    border-radius: 50%;
    width: 24px;
    height: 24px;
    padding: 5.5px;
    background: #fff;
    border: 1px solid #0067C5;
    color: #0067C5;
    font: 10px Arial, sans-serif;
}

.line {
    margin-top: 10px;
    height: 1px;
    background: #0067C5;
}

.circle-done {
    border-radius: 50%;
    width: 24px;
    height: 24px;
    padding: 5.5px;
    background: #005B82;
    border: 1px solid #005B82;
    color: #fff;
    font: 10px Arial, sans-serif;
}

.card {
    margin-bottom: 0;
}
</style>
@endsection
@section('head_content')
@endsection
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="py-2">
            <div class="bg-white border">
                <div class="row">
                    <div class="col-md-8 mx-auto">
                        <div class="row p-3">
                            <div class="col-xs-1">
                                <div class="rounded">
                                    <div class="circle text-center">1</div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="line"></div>
                            </div>
                            <div class="col-xs-1">
                                <div class="rounded">
                                    <div class="circle text-center">2</div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="line"></div>
                            </div>
                            <div class="col-xs-1">
                                <div class="rounded">
                                    <div class="circle-done text-center">3</div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="line"></div>
                            </div>
                            <div class="col-xs-1">
                                <div class="rounded">
                                    <div class="circle-done text-center">4</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-2">
            <div class="p-2 bg-white border">
                <h2>Formulir</h2>
                <h3><b>PPDB2022-TK</b></h3>
            </div>
            <div class="accordion" id="accordionFormulir">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse"
                                data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <div class="row">
                                    <div class="col">
                                        1. PPDB2022-TK
                                    </div>
                                    <div class="col ">
                                        <span class="text-green float-right"><i
                                                class="fa-regular fa-circle-check"></i>Sudah Terpenuhi</span>
                                    </div>
                                </div>
                            </button>
                        </h2>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                        data-parent="#accordionFormulir">

                        <div class="card-body">
                            <!-- data kosong -->
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left collapsed" type="button"
                                data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                aria-controls="collapseTwo">
                                2. Pengisian Data
                            </button>
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                        data-parent="#accordionFormulir">
                        <div class="card-body">
                            <form>
                                <div class="form-row align-items-center">
                                    <div class="col-sm-3 my-1">
                                        <div class="form-group">
                                            <label for="inputFullname">Nama Lengkap</label><code>*</code>
                                            <input type="text" class="form-control" name="inputFullname"
                                                id="inputFullname">
                                        </div>
                                    </div>
                                    <div class="col-sm-3 my-1">
                                        <div class="form-group">
                                            <label for="inputEmail">Email</label><code>*</code>
                                            <input type="email" class="form-control" name="inputEmail" id="inputEmail"
                                                aria-describedby="emailHelp">
                                        </div>
                                    </div>
                                    <div class="col-sm-3 my-1">
                                        <div class="form-group">
                                            <label for="inputRegistrasi">Nomor Registrasi</label><code>*</code>
                                            <input type="text" class="form-control" name="inputRegistrasi"
                                                id="inputRegistrasi">
                                        </div>
                                    </div>
                                    <div class="col-sm-3 my-1">
                                        <div class="form-group">
                                            <label for="inputHp">No.Handphone</label><code>*</code>
                                            <input type="password" class="form-control" name="inputHp" id="inputHp">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row align-items-center">
                                    <div class="col-sm-3 my-1">
                                        <div class="form-group">
                                            <label for="formFile" class="form-label">Foto Diri</label><code>*</code>
                                            <input class="" type="file" id="formFile">
                                        </div>
                                    </div>
                                    <div class="col-sm-3 my-1">
                                        <div class="form-group">
                                            <label for="inputAgama">Agama</label><code>*</code>
                                            <input type="text" class="form-control" name="inputAgama" id="inputAgama">
                                        </div>
                                    </div>
                                    <div class="col-sm-3 my-1">
                                        <div class="form-group">
                                            <label for="inputTempatLahir">Tempat Lahir</label><code>*</code>
                                            <input type="text" class="form-control" name="inputTempatLahir"
                                                id="inputTempatLahir">
                                        </div>
                                    </div>
                                    <div class="col-sm-3 my-1">
                                        <div class="form-group">
                                            <label>Date:</label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" id="datepicker">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="checkTerm">
                                    <label class="form-check-label" for="checkTerm">
                                        Saya menyetujui ketentuan dan persyaratan yang berlaku. <a href="#">Syarat
                                            dan kondisi</a>
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left collapsed" type="button"
                                data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                aria-controls="collapseThree">
                                3. Seleksi
                            </button>
                        </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                        data-parent="#accordionFormulir">
                        <div class="card-body">
                            And lastly, the placeholder content for the third and final accordion panel. This panel
                            is hidden by default.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left collapsed" type="button"
                                data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                aria-controls="collapseThree">
                                4. Tinjauan
                            </button>
                        </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingFour"
                        data-parent="#accordionFormulir">
                        <div class="card-body">
                            And lastly, the placeholder content for the third and final accordion panel. This panel
                            is hidden by default.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@endsection

@section('scripts')
<script src="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script>
$(document).ready(function() {
    $("#sidebarCollapse > li").removeClass("active");
    $("#nav-formulir > a").addClass("active");

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

});

$('[data-toggle="tooltip"]').tooltip()
</script>
@endsection