<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-bell"></i>
                <span class="badge badge-warning navbar-badge">15</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-item dropdown-header">15 Notifications</span>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-users mr-2"></i> 8 friend requests
                    <span class="float-right text-muted text-sm">12 hours</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-file mr-2"></i> 3 new reports
                    <span class="float-right text-muted text-sm">2 days</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
            </div>
        </li>
        <li class="nav-item">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                <button type="button" class="btn dropdown-toggle grey-font" style="color:#869099" data-toggle="dropdown">
                    Nama Calon Siswa
                </button>
                <ul class="dropdown-menu">
                    <li class="dropdown-item grey-font"><i class="fas fa-cog"></i> Pengaturan</li>
                    <li class="dropdown-item grey-font"><i class="fas fa-key"></i> Ganti Password</li>
                    <li class="dropdown-divider"></li>
                    <a href="{{url('logout')}}">
                        <li class="dropdown-item grey-font">
                            <i class="fas fa-sign-out-alt"></i> Logout
                        </li>
                    </a>
                </ul>
                </div>
            </div>
        </li>
    </ul>

</nav>
<!-- /.navbar -->

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <div class="brand-link">
        
        <img width="100%" src="{{asset('dist/img/logo/studenttrackTr.png')}}" alt="alhalimiyah">
    </div>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul id="sidebarCollapse" class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <li class="nav-item" id="nav-home">
                    <a href="{{url('/ppdb/home')}}" class="nav-link">
                        <i class="nav-icon fa-solid fa-home"></i>
                        <p> 
                            Home
                        </p>
                    </a>
                </li>
                <li class="nav-item" id="nav-formulir">
                    <a href="{{url('/ppdb/formulir')}}" class="nav-link">
                        <i class="nav-icon fas fa-user-check"></i>
                        <p>
                            Formulir
                        </p>
                    </a>
                </li>
                <!-- <li class="nav-item bg-secondary" id="nav-nilai">
                    <a href="{{url('#')}}" class="nav-link">
                        <i class="nav-icon far fa-star"></i>
                        <p>
                            lainnya
                        </p>
                    </a>
                </li> -->
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
