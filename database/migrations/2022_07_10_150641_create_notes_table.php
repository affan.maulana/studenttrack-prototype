<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->id();
            $table->string('title',150)->default('');
            $table->string('context')->default('');
            $table->dateTime('date');
            $table->unsignedBigInteger('absent_id')->nullable()->default(0);
            $table->unsignedBigInteger('student_id')->nullable()->default(0)->index('student_id');
            $table->unsignedBigInteger('group_id')->nullable()->default(0)->index('group_id');
            $table->string('created_by',150)->default('');
            $table->string('updated_by',150)->default('');
            $table->timestamps();

            $table->foreign('absent_id')->references('id')->on('absents');
            $table->foreign('student_id')->references('id')->on('students');
            $table->foreign('group_id')->references('id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notes');
    }
}
