<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAbsentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absents', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->unsignedBigInteger('student_id')->nullable()->default(0)->index('student_id');
            $table->unsignedBigInteger('class_id')->nullable()->default(0)->index('class_id');
            $table->unsignedBigInteger('group_id')->nullable()->default(0)->index('group_id');
            $table->string('notes')->nullable()->default('');
            $table->string('created_by',150)->default('');
            $table->string('updated_by',150)->default('');
            $table->timestamps();

            $table->foreign('student_id')->references('id')->on('students');
            $table->foreign('class_id')->references('id')->on('classes');
            $table->foreign('group_id')->references('id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absents');
    }
}
