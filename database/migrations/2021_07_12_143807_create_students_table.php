<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('name',150)->default('');
            $table->unsignedBigInteger('group_id')->nullable()->default(0)->index('group_id');
            $table->unsignedBigInteger('class_id')->nullable()->default(0)->index('class_id');
            $table->string('gender',15)->default('');
            $table->string('created_by',150)->default('');
            $table->string('updated_by',150)->default('');
            $table->timestamps();

            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('class_id')->references('id')->on('classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
