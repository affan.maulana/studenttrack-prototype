<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class DefaultUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [ 
            [
                'email' => 'superadmin@studenttrack.id',
                'name' => 'Super Admin',
                'password' => Hash::make("12qwaszx"),
                'group_id' => 1
            ]
        ];

        foreach ($users as $key => $value) {
            User::create([
                'password' => $value['password'],
                'name' => $value['name'],
                'email' => $value['email'],
                'group_id' => $value['group_id'],
            ]);
        }
    }
}
