<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Groups;

class DefaultGroup extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Groups::create([
            "name" => "Administrator Access",
            "created_by" => "System",
            'updated_by' => "System"
        ]);
    }
}
