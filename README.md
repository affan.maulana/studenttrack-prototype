# Studenttrack Prototype
Bismillah studenttrack prototype v.1

## Requirements
~ PHP >8.0
~ Mysql
~ Laravel 8.7
~ Composer

## Setup Steps
Go to terminal and follow these steps:

```
git clone https://gitlab.com/affan.maulana/studenttrack-prototype.git
Copy .env.example to .env then set your database credentials
composer update
php artisan optimize
php artisan migrate
Php artisan db:seed
php artisan key:generate
```