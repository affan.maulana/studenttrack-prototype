<?php

namespace App\Http\Controllers\auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login()
    {
        if (Session::get('isLogin')) {
            return redirect('/');
        }
        return view('auth/login');
    }

    public function login_ppdb()
    {
        if (Session::get('isLogin')) {
            return redirect('/');
        }
        return view('auth/login_ppdb');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        
        if (Auth::attempt($credentials)) {
            Session::regenerate();
            session(['isLogin' => true]);
            return redirect()->intended('/');
        }
        
        return back()->with('error','User tidak ditemukan');
    }

    public function logout(Request $request)
    {
        Session::invalidate();
        Session::flush();
        return redirect()->intended('login');
    }
}
