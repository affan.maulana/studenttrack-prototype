<?php

namespace App\Http\Controllers\ppdb;

use App\Http\Helper\LogHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;

class PpdbController extends Controller
{
    
    public function index()
    {
        try {
            return view('ppdb/home_ppdb');
        }catch (Exception $e) {
            $id = LogHandler::createLog('exceptions', 'ppdb.index', $e, 'error');
            return redirect()->intended('/')->with('error','#'.$id.' - '.$e->getMessage());
        }
    }

    public function formulir()
    {
        try {
            return view('ppdb/formulir_ppdb');
        }catch (Exception $e) {
            $id = LogHandler::createLog('exceptions', 'ppdb.formulir', $e, 'error');
            return redirect()->intended('/')->with('error','#'.$id.' - '.$e->getMessage());
        }
    }
    
}
