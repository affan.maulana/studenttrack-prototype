<?php

namespace App\Http\Controllers\attendance;

use App\Http\Helper\LogHandler;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use App\Models\Classes;

class AttendanceController extends Controller
{
    
    public function index()
    {
        try {
            $list= Classes::where('group_id', '1')->get();
            $data['list_kelas'] = $list;
            $data['date'] =  date("Y - m - d");
            return view('attendance/attendance')->with('data',$data);
        }catch (Exception $e) {
            $id = LogHandler::createLog('exceptions', 'attendance.index', $e, 'error');
            return redirect()->intended('/')->with('error','#'.$id.' - '.$e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $list= Classes::where('group_id', '1')->get();
            $data['list_kelas'] = $list;
            return view('attendance/student_list')->with('data',$data);
        }catch (Exception $e) {
            $id = LogHandler::createLog('exceptions', 'Attendance.index', $e, 'error');
            return redirect()->intended('/')->with('error','#'.$id.' - '.$e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
