<?php

namespace App\Http\Helper;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Exception;

class LogHandler
{
    public static function createLog($fileName = 'newLog', $logName = 'thisLogIs', $logs = null, $type = 'debug') 
    {
        $id = round(microtime(true));

        $thn = date('Y');
        $bln = date('m');
        $today = date('-Ymd');

        $path = storage_path() . '/logs/' .  $thn . '/' . $bln . '/';
        $tags = explode('/', $path);
        $mkDir = "";

        foreach ($tags as $dir) {
            $mkDir = $mkDir . $dir . "/";
            if (!is_dir($mkDir)) {
                mkdir($mkDir);
                try{
                    chmod($mkDir, 0777);
                }catch(Exception $e){
                    
                }
            }
        }

        $logger = new Logger($logName.'#'.$id);
        $logLocation = $path . $fileName . $today . '.log';
        $logger->pushHandler(new StreamHandler($logLocation), Logger::DEBUG);

        if($type == 'error'){
            $logger->error('Message: '.$logs->getMessage() .' | File: '.$logs->getFile().' | Line: '.$logs->getLine());
        }else{
            $logger->debug($logs);
        }

        return $id;

    }
}
